# Mars Rover Problem

## Running the solution
Hi Red Badger!

To get started first run:
~~~
npm install
~~~
and then run
~~~
npm start
~~~
Finally, Open your browser at http://localhost:3000 to see the solution

## Running the tests
To run the tests do:
~~~
npm run test
~~~

## Technologies used:
React, Redux, ES6, Mocha, Chai, Webpack

## Special Notes:
I made an assumption that the rover would just ignore an instruction to move into a space occupied by another rover
