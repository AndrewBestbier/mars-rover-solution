
import {assert} from 'chai';
import {Map} from 'immutable';
import {orientationStringToInteger, orientationIntegerToString, checkRoverInBounds, rotateRover, moveRover} from '../src/reducers/planet';

describe('orientationStringToInteger', () => {
  it('returns the correct orientation integer when passed the respective string', () => {
    assert.equal(orientationStringToInteger('N'),0);
    assert.equal(orientationStringToInteger('E'),1);
    assert.equal(orientationStringToInteger('S'),2);
    assert.equal(orientationStringToInteger('W'),3);
  });
});

describe('orientationIntegerToString', () => {
  it('returns the correct orientation string when passed the respective integers', () => {
    assert.equal(orientationIntegerToString(0),'N');
    assert.equal(orientationIntegerToString(1),'E');
    assert.equal(orientationIntegerToString(2),'S');
    assert.equal(orientationIntegerToString(3),'W');
  });
});

describe('checkRoverInBounds', () => {
  it('returns true if the rover is within bounds', () => {
    let rover = Map({
      xCord: 1,
      yCord: 2
    });
    let state = Map({
      xBound: 10,
      yBound: 20
    });
    assert.equal(checkRoverInBounds(rover, state), true);
  });

  it('returns false if the rover is NOT within bounds', () => {
    let rover = Map({
      xCord: -1,
      yCord: 2
    });
    let state = Map({
      xBound: 10,
      yBound: 20
    });
    assert.equal(checkRoverInBounds(rover, state), false);
  });
});

describe('rotateRover', () => {
  describe('clockwise (R) rotations', () => {
    it('returns a rover with the correct clockwise rotation value', () => {
      assert.equal(rotateRover(Map({orientationInteger: 0}), 'R').toJS().orientationInteger,1);
      assert.equal(rotateRover(Map({orientationInteger: 1}), 'R').toJS().orientationInteger,2);
      assert.equal(rotateRover(Map({orientationInteger: 2}), 'R').toJS().orientationInteger,3);
      assert.equal(rotateRover(Map({orientationInteger: 3}), 'R').toJS().orientationInteger,0);
    });
  });
  describe('anti-clockwise (L) rotations', () => {
    it('returns a rover with the correct clockwise rotation value', () => {
      assert.equal(rotateRover(Map({orientationInteger: 0}), 'L').toJS().orientationInteger,3);
      assert.equal(rotateRover(Map({orientationInteger: 1}), 'L').toJS().orientationInteger,0);
      assert.equal(rotateRover(Map({orientationInteger: 2}), 'L').toJS().orientationInteger,1);
      assert.equal(rotateRover(Map({orientationInteger: 3}), 'L').toJS().orientationInteger,2);
    });
  });
});

describe('moveRover', () => {
  it('returns the correct new co-ordinate for a rover with a given direction', () => {
    /* North */
    assert.equal(moveRover(Map({orientationInteger: 0, xCord: 1, yCord: 2})).toJS().xCord,1);
    assert.equal(moveRover(Map({orientationInteger: 0, xCord: 1, yCord: 2})).toJS().yCord,3);
    /* East */
    assert.equal(moveRover(Map({orientationInteger: 1, xCord: 1, yCord: 2})).toJS().xCord,2);
    assert.equal(moveRover(Map({orientationInteger: 1, xCord: 1, yCord: 2})).toJS().yCord,2);
    /* South */
    assert.equal(moveRover(Map({orientationInteger: 2, xCord: 1, yCord: 2})).toJS().xCord,1);
    assert.equal(moveRover(Map({orientationInteger: 2, xCord: 1, yCord: 2})).toJS().yCord,1);
    /* West */
    assert.equal(moveRover(Map({orientationInteger: 3, xCord: 1, yCord: 2})).toJS().xCord,0);
    assert.equal(moveRover(Map({orientationInteger: 3, xCord: 1, yCord: 2})).toJS().yCord,2);
  });
});
