import React from 'react';
import ReactDOM from 'react-dom';
import './styles/main.scss';
import configureStore from './store/configureStore';
import App from './containers/App';
const store = configureStore();

ReactDOM.render(
  <App store={store} />,
  document.getElementById('root')
);
