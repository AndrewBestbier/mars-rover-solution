import React, { Component, PropTypes } from 'react';

export default class Input extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      input: ''
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({input: event.target.value});
  }

  handleInputSubmit() {
    this.props.actions.executeInstructions(this.state.input);
  }

  render() {
    return (
      <div>
        <h3>Input:</h3>
        <textarea
          className='u-full-width'
          value={this.state.value}
          onChange={this.handleChange}
        />
        <div><button className="button-primary" onClick={() => {this.handleInputSubmit();}}>Execute Instructions</button></div>
      </div>
    );
  }
}

Input.propTypes = {
  planet: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};
