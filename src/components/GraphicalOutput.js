import React, { Component, PropTypes } from 'react';

export default class GraphicalOutput extends Component {
  constructor(props, context) {
    super(props, context);
  }

  getCellClassForRoverOrientation(orientationIndex){
    if(orientationIndex === 0){
      return 'cell-rover-north';
    }
    if(orientationIndex === 1){
      return 'cell-rover-east';
    }
    if(orientationIndex === 2){
      return 'cell-rover-south';
    }
    if(orientationIndex === 3){
      return 'cell-rover-west';
    }
  }

  getCells(row){
    return row.map((cell, j) => {
      return (
        <td className={this.getCellClassForRoverOrientation(cell)} key={j}></td>
      );
    });
  }

  getRows(data){
    return data.reverse().map((row, i) => {
      return (
        <tr key={i}>{this.getCells(row)}</tr>
      );
    });
  }

  render() {
    return (
      <div>
        <table>
          <tbody>
            {this.getRows(this.props.planet.get('planetMatrix'))}
          </tbody>
      </table>
      </div>
    );
  }
}

GraphicalOutput.propTypes = {
  planet: PropTypes.object.isRequired
};
