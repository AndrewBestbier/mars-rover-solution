import React, { Component, PropTypes } from 'react';

export default class Output extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <h3>Output:</h3>
        <textarea
          className='u-full-width'
          value={this.props.planet.get('output')}
        />
      </div>
    );
  }
}

Output.propTypes = {
  planet: PropTypes.object.isRequired
};
