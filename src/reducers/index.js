import { combineReducers } from 'redux';
import planet from './planet';

const rootReducer = combineReducers({
  planet
});

export default rootReducer;
