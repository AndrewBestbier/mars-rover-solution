import {Map, fromJS, List} from 'immutable';

const initialState = Map({
  activeRover: Map({}), //The Rover that is being actively manipulated
  xBound: 0, //The X value for the boundry of the planet
  yBound: 0, //The Y value for the boundry of the planet
  planetMatrix: List(), //A two dimensional array containing the positions of all rovers
  output: '', //The output text
  blockedLocations: List() //Co-ordinates of scents and other robots
});

/**
 * Returns true or false depending on whether the rover is positioned outside the bounds of the map
 * @param {Map} rover
 * @param {Map} state
 * @return {Boolean}
 */
export function checkRoverInBounds(rover, state){
  if(rover.get('xCord') > state.get('xBound') || rover.get('yCord') > state.get('yBound') || rover.get('xCord') < 0 || rover.get('yCord') < 0){
    return false;
  }
  return true;
}

/**
 * Returns the corresponding orientationInteger when provided with an orientationString
 * @param {String} orientationString
 * @return {Number} orientationInteger
 */
export function orientationStringToInteger(orientationString){
  switch(orientationString){
  case 'N':
    return 0;
  case 'E':
    return 1;
  case 'S':
    return 2;
  case 'W':
    return 3;
  }
}

/**
 * Returns the corresponding orientationString when provided with an orientationInteger
 * @param {Number} orientationInteger
 * @return {String} orientationString
 */
export function orientationIntegerToString(orientationString){
  switch(orientationString){
  case 0:
    return 'N';
  case 1:
    return 'E';
  case 2:
    return 'S';
  case 3:
    return 'W';
  }
}

/**
 * Returns an updated rover with it's new orientation direction when passed a rotation direction
 * @param {Map} rover
 * @return {String} rotationDirection (R or L)
 */
export function rotateRover(rover, rotationDirection){
  /* Updating the orientation of the rover and then returning it */
  return rover.update('orientationInteger', currentRoverOrientation => {
    if(rotationDirection === 'R'){
      /**
       * In this case, the instruction is to rotate the rover 90 degrees clockwise.
       * This is achieved by adding 1 to the current orientation
       * So N->E (0->1), E->S (1->2), S->W (2->3), W->N (3->0)
       */
      return (currentRoverOrientation += 1) % 4; //Modulus to limit range from 0 to 3
    }
    /**
     * In this case, the instruction is to rotate the rover 90 degrees counter-clockwise.
     * This is achieved by adding 3 to the current orientation (Note the circularity enabled by the modulus)
     * So N->W (0->3), E->N (1->0), S->E (2->1), W->S (3->2)
     */
    return (currentRoverOrientation += 3) % 4; //Modulus to limit range from 0 to 3
  });
}

/**
 * Returns an updated rover with it's co-ordinates
 * @param {Map} rover
 * @return {Map} rover with new co-ordinates
 */
export function moveRover(rover){
  let currentRoverOrientation = rover.get('orientationInteger');
  if(currentRoverOrientation % 2 === 0){
    /* The orientation of the rover is either North or South */
    return rover.update('yCord', previousYCord => {
      /**
       * As the rover is facing either North (0) or South (2), only the Y value will change
       * If the currentOrientation is North (0), the equation becomes: previousYCord + 1
       * If the currentOrientation is South (2), the equation becomes: previousYCord - 1
       */
      return previousYCord - currentRoverOrientation + 1;
    });
  } else {
    /* The orientation of the rover is either East or West */
    return rover.update('xCord', previousXCord => {
      /**
       * As the rover is facing either East (1) or West (3), only the X value will change
       * If the currentOrientation is East (1), the equation becomes: previousXCord + 1
       * If the currentOrientation is West (3), the equation becomes: previousXCord - 1
       */
      return previousXCord - currentRoverOrientation + 2;
    });
  }
}

export default function planet(state = initialState, action = {}) {
  const { data, type } = action;
  switch (type) {
  case 'SETUP_PLANET':
    /*
     * The first step is a space separated string like: "1 3".
     * This defines the planet bounds for X and Y respectively
    */
    let bounds = data.split(' ');
    let xBound = parseInt(bounds[0]);
    let yBound = parseInt(bounds[1]);
    /* A two dimensional array the sizes of the specified bounds is then populated with empty values */
    let emptyPlanet = Array(yBound + 1).fill().map(()=>Array(xBound + 1).fill(''));

    /* Returning the updated state */
    return initialState.merge({
      xBound: xBound,
      yBound: yBound,
      planetMatrix: fromJS(emptyPlanet) //Note the fromJS that converts the empty planet matrix to an immutable List
    });
  case 'ADD_ROVER':
    /*
     * The instructions to add a rover look like: "1 3 E".
     * This defines the rovers initial co-ordinates and orientation respectively
    */
    let newRoverSpecificationArray = data.split(' ');
    let xCord = parseInt(newRoverSpecificationArray[0]);
    let yCord = parseInt(newRoverSpecificationArray[1]);
    let newRover = Map({
      xCord: xCord,
      yCord: yCord,
      /*
       * We convert the orientation string (N,E,S,W) to integers here (0,1,2,3).
       * This is done for the mathematical calculations when moving or rotating the rovers
      */
      orientationInteger: orientationStringToInteger(newRoverSpecificationArray[2]),
      state: '' //The state of the rover is either empty or LOST
    });
    return state.merge({
      activeRover: newRover, //Sets our newly added rover as the active rover to be manipulated
      /* Updating our planet matrix with the position of the new rover */
      planetMatrix: state.get('planetMatrix').updateIn([yCord,xCord], () => newRover.get('orientationInteger'))
    });
  case 'ROTATE_ROVER':
    /* If the rover is lost, do nothing */
    if(state.get('activeRover').get('state') === 'LOST'){
      return state;
    }
    /* Updating the activeRover with the new orientation value */
    let rotatedRover = rotateRover(state.get('activeRover'), data);

    return state.merge({
      activeRover: rotatedRover,
      /* Updating the planetMatrix with the new rover orientation */
      planetMatrix: state.get('planetMatrix')
      /* Updating the matrix at [Y,X] not [X,Y]. This is done because the graphical mars Table
       * is generated with [0,0] in the top left, not the bottom left. Thus we invert the data here
       */
      .updateIn([state.get('activeRover').get('yCord'),state.get('activeRover').get('xCord')], () => {
        /* Setting the previous value to the orientation integer */
        return rotatedRover.get('orientationInteger');
      })
    });
  case 'MOVE_ROVER':
    /* If the rover is lost, do nothing */
    if(state.get('activeRover').get('state') === 'LOST'){
      return state;
    }

    /* Getting a rover with the new co-ordinate value when moved */
    let movedRover = moveRover(state.get('activeRover'));

    /* Checking if the co-ordinates of the movedRover are blocked off */
    let result = state.get('blockedLocations').findIndex((location) => {
      if(location.equals(movedRover)){
        return location;
      }
      return;
    });
    /* If there is a scent or another rover, the active rover simply ignores the instruction */
    if(result != -1){
      return state;
    }

    /* If the rover is in bounds, then move the rover on the map and remove the old positioning */
    if(checkRoverInBounds(movedRover, state)){
      return state.merge({
        activeRover: movedRover,
        planetMatrix: state.get('planetMatrix')
          /* Updating the planetMatrix with the new rover position */
          .updateIn([movedRover.get('yCord'),movedRover.get('xCord')], () => movedRover.get('orientationInteger'))
          /* Removing the position of the old rover */
          .updateIn([state.get('activeRover').get('yCord'),state.get('activeRover').get('xCord')], () => '')
      });
    }
    /* If the rover is NOT in bounds, remove it from the map, update the state of the rover, and add a scent */
    return state.merge({
      planetMatrix: state.get('planetMatrix')
        /* Removing the rover from the matrix */
        .updateIn([state.get('activeRover').get('yCord'),state.get('activeRover').get('xCord')], () => ''),
      /* Setting the rover state to lost */
      activeRover: state.get('activeRover').update('state', () => 'LOST'),
      /* Adding the would-be co-oridinates to a blockedLocations array as a preventative scent for other robots */
      blockedLocations: state.get('blockedLocations').push(movedRover)
    });
  case 'PRINT_ROVER_LOCATION':
    /* This is called when the rover has finished moving */
    let activeRover = state.get('activeRover').toJS();
    let newOutputLine = activeRover.xCord + ' ' +  activeRover.yCord + ' ' +  orientationIntegerToString(activeRover.orientationInteger) + ' ' + activeRover.state + '\n';
    return state.merge({
      /* Adding the position of the rover to blockedLocations so other robots cannot collide with it */
      blockedLocations: state.get('blockedLocations').push(state.get('activeRover')),
      /* Outputting the robot's final position and state */
      output: state.get('output') + newOutputLine
    });
  default:
    return state;
  }
}
