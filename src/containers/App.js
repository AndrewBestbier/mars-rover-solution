import React, { Component, PropTypes } from 'react';

/* Redux Imports */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

/* Actions */

import * as PlanetActions from '../actions/PlanetActions';
/* Component Imports */
import Input from '../components/Input';
import Output from '../components/Output';
import GraphicalOutput from '../components/GraphicalOutput';

export default class App extends Component {
  render() {
    const { planet, actions } = this.props;

    return (
      <div className='container'>
        <div className='row'>
          <div className='twelve columns'>
            <Input planet={planet} actions={actions} />
          </div>
          <div className='twelve columns'>
            <Output planet={planet} />
          </div>
          <div>
            <GraphicalOutput planet={planet} />
          </div>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  planet: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

/* Connecting Component to Redux */
function mapStateToProps(state) {
  return {
    planet: state.planet
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(PlanetActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
