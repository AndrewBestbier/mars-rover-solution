import async from 'async';

export function executeInstructions(instructions) {
  /* Splitting the instructions into an array by line. The empty values are filtered out */
  let splitInstructions = instructions.trim().split(/\r?\n/).filter((instruction) => instruction !== '');
  /* Redux-thunk is used here so that multiple actions can be dispatched */
  return dispatch => {
    async.forEachOfSeries(splitInstructions, (instruction, index, done) => {
      if(index === 0){
        /* The first instruction in the array is the specifications of the planet */
        dispatch(setupPlanet(instruction));
        done();
      }
      if(index % 2 !== 0) {
        /* If the index is odd, the instruction is the setup instructions of a new Rover */
        dispatch(addRover(instruction));
        done();
      }
      if(index % 2 === 0 && index !== 0){
        /* If the index is even, the instruction is the execution instructions for a previously Created Rover */
        instructRover(dispatch, instruction, done);
      }
    });
  };
}

function instructRover(dispatch, instruction, done){
  let actions = instruction.split('');
  actions.forEach((action, index) => {
    /* Timer is set here to show the rovers moving graphically */
    setTimeout(function () {
      if(action === 'F'){
        dispatch(moveRover());
      } else {
        dispatch(rotateRover(action));
      }
      if(index === actions.length -1){
        /* If there are no more actions, print the rover location and move onto the next instruction */
        dispatch(printRoverLocation());
        done();
      }
    }, 1000 * (index + 1));
  });
}

function setupPlanet(instruction){
  return {
    type: 'SETUP_PLANET',
    data: instruction
  };
}

function printRoverLocation(){
  return {
    type: 'PRINT_ROVER_LOCATION'
  };
}

function addRover(instruction){
  return {
    type: 'ADD_ROVER',
    data: instruction
  };
}

function rotateRover(action){
  return {
    type: 'ROTATE_ROVER',
    data: action
  };
}

function moveRover(){
  return {
    type: 'MOVE_ROVER'
  };
}
